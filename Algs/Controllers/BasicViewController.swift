//
//  BasicViewController.swift
//  Algs
//
//  Created by THXDBase on 09.02.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import UIKit

class BasicViewController: UIViewController {
    
    /// Numpad's input accessory
    lazy var doneToolbar: UIToolbar = {
        let bar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 35))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(BasicViewController.onKeyboardDoneHandler))
        bar.items = [space, doneBtn]
        bar.sizeToFit()
        return bar
    }()
    
    @objc func onKeyboardDoneHandler(_ sender: UIBarButtonItem) {}
}
