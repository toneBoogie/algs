//
//  Algorithm.swift
//  Algs
//
//  Created by THXDBase on 31.01.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import Foundation

enum Algorithm: CustomStringConvertible {

    case bubble(Double)
    case select(Double)
    case merge(Double)
    
    init?(from: Int) {
        switch from {
        case 0:
            self = .bubble(0)
        case 1:
            self = .select(0)
        case 2:
            self = .merge(0)
        
        default:
            return nil
        }
    }
    
    // TODO: provide the Generic type
    // Also, I skipped the array count check because it is implemented at UI level
    func sorted(_ array: [Int]) -> [Int] {
        var result = array
        switch self {
        case .bubble:
            // MARK: - BUBBLE
            for i in 0..<result.count {
                for j in 1..<result.count - i {
                    if result[j] < result[j - 1] {
                        let tmp = result[j - 1]
                        result[j - 1] = result[j]
                        result[j] = tmp
                    }
                }
            }
        case .select:
            // MARK: - SELECT
            for i in 0..<result.count {
                var lowest = i
                for j in i + 1 ..< result.count{
                    if result[j] < result[lowest] {
                        lowest = j
                    }
                }
                if i != lowest {
                    result.swapAt(i, lowest)
                }
            }
        case .merge:
            // MARK: - MERGE. bottom-up.
            let n = result.count
            var z = [result, result]
            var d = 0
            var width = 1
            
            while width < n {
                var i = 0
                
                while i < n {
                    var j = i
                    var l = i
                    var r = i + width
                    
                    let lMax = min(l + width, n)
                    let rMax = min(r + width, n)
                    
                    while l < lMax && r < rMax {
                        if z[d][l] < z[d][r] {
                            z[1 - d][j] = z[d][l]
                            l += 1
                        } else {
                            z[1 - d][j] = z[d][r]
                            r += 1
                        }
                        j += 1
                    }
                    while l < lMax {
                        z[1 - d][j] = z[d][l]
                        j += 1
                        l += 1
                    }
                    while r < rMax {
                        z[1 - d][j] = z[d][r]
                        j += 1
                        r += 1
                    }
                    i += width * 2
                }
                width *= 2
                d = 1 - d
            }
            result = z[d]
        }
        return result
    }
    
    // MARK: - CustomStringConvertible
    var description: String {
        switch self {
        case .bubble:
            return "Bubble"
        case .select:
            return "Select"
        case .merge:
            return"Merge"
        }
    }
    //
    /// Will return an assotiated property
    var result: Double {
        switch self {
        case .bubble(let result):
            return result
        case .select(let result):
            return result
        case .merge(let result):
            return result
        }
    }
    /// Will return a formatted assotiated property
    var resultDescription: String {
        return self.description + ": " + String(self.result) + " s"
    }
    
    // I decided to avoid a mutating func here for now.
    /// Will return the same enum with set value
    func setResult(_ val: Double) -> Algorithm {
        switch self {
        case .bubble:
            return .bubble(val)
        case .select:
            return .select(val)
        case .merge:
            return .merge(val)
        }
    }
    
    //
    static var all: [Algorithm] {
        return [.bubble(0), .select(0), .merge(0)]
    }
}
