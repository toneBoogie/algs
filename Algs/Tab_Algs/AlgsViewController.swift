//
//  AlgsViewController.swift
//  Algs
//
//  Created by THXDBase on 30.01.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import UIKit

final class AlgsViewController: BasicViewController, AlgsDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var sizeField: UITextField!
    @IBOutlet weak var repeatsField: UITextField!
    @IBOutlet weak var bubbleSwitcher: UISwitch!
    @IBOutlet weak var selectSwitcher: UISwitch!
    @IBOutlet weak var mergeSwitcher: UISwitch!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    /// As we have a hardcoded Switchers for now, lets keep them here.
    lazy private var hardcodedSwitchers: [UISwitch] = {
       return  [bubbleSwitcher, selectSwitcher, mergeSwitcher]
    }()
    
    // MARK: - MODEL
    private let model: AlgsModel = AlgsModel()
    
    // MARK: - AlgsDelegate
    var selections: [Algorithm] = [] {
        didSet{
            setupResultsStack()
            startButton.isEnabled = selections.count > 0
        }
    }
    var arrayCount: Int = 1000 {
        didSet{
            sizeField.text = arrayCount.description
        }
    }
    var repeatsCount: Int = 5 {
        didSet{
            repeatsField.text = repeatsCount.description
        }
    }
    func showLoader() {
        let currentFrame = stackView.bounds
        // clear
        for arrangedSubView in stackView.arrangedSubviews {
            stackView.removeArrangedSubview(arrangedSubView)
            arrangedSubView.removeFromSuperview()
        }
        let loader = UIActivityIndicatorView(style: .large)
        loader.color = .black
        loader.frame = currentFrame
        stackView.addSubview(loader)
        loader.startAnimating()
    }
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        // switchers
        bubbleSwitcher.addTarget(self, action: #selector(onSwitchChanged), for: .valueChanged)
        selectSwitcher.addTarget(self, action: #selector(onSwitchChanged), for: .valueChanged)
        mergeSwitcher.addTarget(self, action: #selector(onSwitchChanged), for: .valueChanged)
        // stack
        stackView.addBackground(color: .white)
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)
        // texts
        sizeField.inputAccessoryView = doneToolbar
        sizeField.delegate = self
        repeatsField.inputAccessoryView = doneToolbar
        repeatsField.delegate = self
        // we need the stack's background added before drawing the results.
        model.delegate = self
        model.setupUI()
    }
    required init?(coder: NSCoder) {
        super.init(nibName: "AlgsViewController", bundle: nil)
        tabBarItem = UITabBarItem(title: "Sort Alg", image: UIImage(systemName: "timer"), tag: 0)
    }
    
    // MARK: - Results Stack
    /// Takes a selected switchers from the model. 
    private func setupResultsStack() {
        // clear
        for arrangedSubView in stackView.arrangedSubviews {
            stackView.removeArrangedSubview(arrangedSubView)
            arrangedSubView.removeFromSuperview()
        }
        for subView in stackView.subviews where subView is UIActivityIndicatorView {
            (subView as? UIActivityIndicatorView)?.stopAnimating()
            subView.removeFromSuperview()
        }
        // draw
        for alg in selections {
            addResult(alg)
        }
        // add an extra view to make a space under results
        let gapView = UIView(frame: view.bounds)
        stackView.addArrangedSubview(gapView)
        //
        baseView.isUserInteractionEnabled = true
        baseView.alpha = 1
    }
    private func addResult(_ result: Algorithm) {
        let view = UIView()
        view.backgroundColor = .green
        
        let label = UILabel()
        label.textColor = .black
        label.text = result.resultDescription
        label.sizeToFit()
        
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5).isActive = true
        label.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        stackView.addArrangedSubview(view)
    }
    
    // MARK: - Handlers
    @objc override func onKeyboardDoneHandler(_ sender: UIBarButtonItem) {
        sizeField.resignFirstResponder()
        repeatsField.resignFirstResponder()
    }
    @objc func onSwitchChanged(_ sender: UISwitch? = nil) {
        let tags = hardcodedSwitchers.filter({ $0.isOn == true }).compactMap({ $0.tag })
        model.updateSelections(tags)
    }
    @IBAction func onStartHandler(_ sender: Any) {
        // UI
        baseView.isUserInteractionEnabled = false
        baseView.alpha = 0.5
        // Measure
        model.startMeasurements()
    }
    
    // MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let inputLocationLimit = textField == sizeField ? 4 : 2
        guard range.location <= inputLocationLimit else {
            // limit an input size
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // clear a field before to input a value
        textField.text = ""
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let result = Int(textField.text ?? "0")
        if textField == sizeField {
            model.arrayCount = result
        } else if textField == repeatsField {
            model.repeatsCount = result
        }
    }
}

// MARK: -
fileprivate extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
