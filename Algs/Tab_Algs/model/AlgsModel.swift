//
//  AlgsModel.swift
//  Algs
//
//  Created by THXDBase on 31.01.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import Foundation

// MARK: - Delegate
protocol AlgsDelegate: class {
    /// All the selected Algorithms
    var selections: [Algorithm] {set get}
    /// The quantity of the array items
    var arrayCount: Int {set get}
    /// The numer of each alg type repeats
    var repeatsCount: Int {set get}
    /// Will hide the measuremet results and show the loader
    func showLoader()
}

// MARK: - Model
final class AlgsModel {
    
    /// Set via *updateSelections*
    private var selectedAlgorithms: [Algorithm] = [.bubble(0), .select(0), .merge(0)]
    var arrayCount: Int? = 1000 {
        didSet{
            if arrayCount == 0 || arrayCount == nil {
                arrayCount = 1000
                delegate?.arrayCount = 1000
            }
        }
    }
    var repeatsCount: Int? = 5 {
        didSet{
            if repeatsCount == 0 || repeatsCount == nil {
                repeatsCount = 5
                delegate?.repeatsCount = 5
            }
        }
    }
    /// The ViewController
    weak var delegate: AlgsDelegate? {
        didSet{
            delegate?.selections = Algorithm.all
        }
    }
    
    // MARK: - API
    func setupUI() {
        delegate?.selections = selectedAlgorithms
        delegate?.arrayCount = arrayCount ?? 1000
        delegate?.repeatsCount = repeatsCount ?? 5
    }
    /// Will set the selected algs
    func updateSelections(_ tags: [Int]) {
        self.selectedAlgorithms = tags.compactMap({ Algorithm(from: $0) })
        delegate?.selections = self.selectedAlgorithms
    }
    /// Will measure the sorts
    func startMeasurements() {
        guard let count = arrayCount
            , let repeats = repeatsCount else {
            fatalError("no variables provided")
        }
        // setup UI
        delegate?.showLoader()
        // measure
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let selectedAlgorithms = self?.selectedAlgorithms else {
                    fatalError("self is lost(")
            }
            // prepare
            let randomArray = Array.getRandom(ofCount: count)
            let queue = DispatchQueue(label: "com.boogie.measurementQueue")
            print("randomArray: ", randomArray)//________________________________________________
            var result: [Algorithm] = []
            let group = DispatchGroup()
            // measure
            for alg in selectedAlgorithms {
                let startTime = Date()
                for _ in 1...repeats {
                    group.enter()
                    queue.async {
                        let sortedArray = alg.sorted(randomArray)
                        print("sortedArray of \(alg.description): ", sortedArray)//______________
                        group.leave()
                    }
                }
                group.wait()
                let time = Date().timeIntervalSince(startTime) / Double(repeats)
                print("\(alg.description) did sort in \(time) s")//______________________________
                let algResult = alg.setResult(time)
                result.append(algResult)
            }
            // send result to UI
            DispatchQueue.main.async {
                self?.delegate?.selections = result
            }
        }
    }
}
