//
//  DemoViewController.swift
//  Algs
//
//  Created by THXDBase on 30.01.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import UIKit

final class DemoViewController: BasicViewController, DemoDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var restartButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var sizeField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    private let cellName = "DemoCell"
    
    //MARK: - MODEL
    private let model = DemoModel()
    
    private var data = TableViewData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // UI
        tableView.backgroundColor = .white
        sizeField.inputAccessoryView = doneToolbar
        sizeField.delegate = self
        // Table View
        tableView.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        tableView.delegate = self
        tableView.dataSource = self
        //
        model.delegate = self
        model.setupUI()
    }
    required init?(coder: NSCoder) {
        super.init(nibName: "DemoViewController", bundle: nil)
        tabBarItem = UITabBarItem(title: "Demo", image: UIImage(systemName: "text.aligncenter"), tag: 1)
    }
    
    // MARK: - DemoDelegate
    var arrayCount: Int = 5 {
        didSet{
            sizeField.text = arrayCount.description
        }
    }
    func setupTableView(with newData: TableViewData) {
        // insert section
        let indexesToRemove = newData.getSectionToRemove(data)
        let indexesToAdd = newData.getSectionToAdd(data)
        tableView.beginUpdates()
        data = newData
        if let deletes = indexesToRemove {
            tableView.deleteSections(deletes, with: .fade)
        }
        if let adds = indexesToAdd {
            tableView.insertSections(adds, with: .fade)
        }
        tableView.endUpdates()
    }
    
    // MARK: - handlers
    @IBAction func onRestartHandler(_ sender: Any) {
        model.setupUI()
    }
    @IBAction func onNextHandler(_ sender: Any) {
        model.next()
    }
    override func onKeyboardDoneHandler(_ sender: UIBarButtonItem) {
        sizeField.resignFirstResponder()
    }
    
    // MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let inputLocationLimit = 1
        guard range.location <= inputLocationLimit else {
            // limit an input size
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // clear a field before to input a value
        textField.text = ""
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let result = Int(textField.text ?? "0")
        model.arrayCount = result
    }
    
    // MARK: - Table View
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.numSections
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.numRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellName) else {
            return UITableViewCell()
        }
        cell.textLabel?.text = data.getCellData(indexPath)
        return cell
    }
}
