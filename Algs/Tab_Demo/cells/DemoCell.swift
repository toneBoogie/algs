//
//  DemoCell.swift
//  Algs
//
//  Created by THXDBase on 10.02.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import UIKit
// TODO: remove nib. I had supposed to provide more complex animations of the sort
class DemoCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = .white
        contentView.backgroundColor = .green
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 2, left: 10, bottom: 2, right: 10))
    }

}
