//
//  DemoModel.swift
//  Algs
//
//  Created by THXDBase on 09.02.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import Foundation

// MARK: - DELEGATE
protocol DemoDelegate: class {
    var arrayCount: Int {set get}
    func setupTableView(with data: TableViewData)
}

// MARK: - MODEL
final class DemoModel {
    private let maxArraySizeCount = 13
    private let minArraySizeCount = 5
    
    /// The Data Source
    private var data = TableViewData()
    /// The ViewController
    weak var delegate: DemoDelegate?
    
    // MARK: - API
    var arrayCount: Int? = 5 {
        didSet{
            guard var count = arrayCount else {
                arrayCount = minArraySizeCount
                delegate?.arrayCount = minArraySizeCount
                return
            }
            count = min(count, maxArraySizeCount)
            count = max(count, minArraySizeCount)
            arrayCount = count
            delegate?.arrayCount = count
        }
    }
    /// Works as Restart and Initial Setup
    func setupUI() {
        delegate?.arrayCount = arrayCount ?? 5
        data.array = Array.getUniqRandom(ofCount: arrayCount ?? 5)
        delegate?.setupTableView(with: data)
    }
    /// The Main step switcher of the MergeSort
    func next() {
        data.next()
        delegate?.setupTableView(with: data)
    }
}
