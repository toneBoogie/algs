//
//  DemoDataSource.swift
//  Algs
//
//  Created by THXDBase on 09.02.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import UIKit

struct TableViewData {
    /// Flag for skip the splitting at *next*
    private var isSplitted = false
    /// Will setup the data
    var array: [Int] = [] {
        didSet{
            isSplitted = false
            data = [array]
        }
    }
    /// Main DataSource
    private var data: [[Int]] = [[]]
    
    var sections: [[Int]] {
        return data
    }
    /// Will return a number of sections
    var numSections: Int {
        return data.count
    }
    /// Will return a number of rows in the section
    func numRows(inSection section: Int) -> Int {
        return data[section].count
    }
    /// Will return the cell's value
    func getCellData(_ indexPath: IndexPath) -> String {
        return data[indexPath.section][indexPath.row].description
    }
    
    func getSectionToRemove(_ oldData: TableViewData) -> IndexSet? {
        var result: [Int] = []
        for (i,section) in oldData.sections.enumerated() {
            if data.firstIndex(of: section) == nil {
                result.append(i)
            }
        }
        return IndexSet(result)
    }
    func getSectionToAdd(_ oldData: TableViewData) -> IndexSet? {
        var result: [Int] = []
        for (i,section) in data.enumerated() {
            if oldData.sections.firstIndex(of: section) == nil {
                result.append(i)
            }
        }
        return IndexSet(result)
    }
    

    // MARK: - MERGE
    mutating func next() {
        // splitting
        if isSplitted == false, let index = data.firstIndex(where: { $0.count > 1 }) {
            let first = data.remove(at: index)
            if let splits = first.split() {
                data.insert(contentsOf: splits, at: index)
            }
            return
        } else {
            isSplitted = true
        }
        // merging
        var mergeCount = 1
        while mergeCount < array.count {
            if let firstIndex = data.firstIndex(where: { $0.count == mergeCount})
                , let secondIndex = data[data.index(after: firstIndex)..<data.count].firstIndex(where: { $0.count == mergeCount}) {
                
                let first = data[firstIndex]
                data[secondIndex].append(contentsOf: first)
                data[secondIndex].sort()
                data.remove(at: firstIndex)
                return
            } else {
                mergeCount += 1
            }
        }
        // final odd
        if numSections == 2 {
            data[0].append(contentsOf: data[1])
            data[0].sort()
            data.remove(at: 1)
        }
    }
    
}
