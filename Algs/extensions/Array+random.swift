//
//  Array+random.swift
//  Algs
//
//  Created by THXDBase on 09.02.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import Foundation

extension Array where Element == Int {
    static func getRandom(ofCount size: Int) -> [Int] {
        var result = [Int]()
        for _ in 0..<size {
            let item = Int.random(in: 1...size)
            result.append(item)
        }
        return result
    }
    static func getUniqRandom(ofCount size: Int) -> [Int] {
        var result = [Int]()
        for _ in 0..<size {
            var item = Int.random(in: 1...size)
            while result.contains(item) {
                item = Int.random(in: 1...size)
            }
            result.append(item)
        }
        return result
    }
}
