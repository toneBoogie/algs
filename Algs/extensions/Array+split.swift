//
//  Array+split.swift
//  Algs
//
//  Created by THXDBase on 10.02.2021.
//  Copyright © 2021 BOOGIE. All rights reserved.
//

import Foundation

extension Array where Element == Int {
    func split() -> [[Int]]? {
        guard count >= 2 else {
            return nil
        }
        let middle = count / 2
        let firstArr = self[0..<middle]
        let secondArr = self[middle..<count]
        return [Array(firstArr),Array(secondArr)]
    }
}
